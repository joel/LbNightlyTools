#!/usr/bin/env python
###############################################################################
# (c) Copyright 2016 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Request for a periodic test to be run

'''
__author__ = 'Ben Couturier <ben.couturier@cern.ch>'

import LbUtils.Script
import sys
import LbMsg.TestMsg

import LbUtils.Log
from LbNightlyTools.Utils import JenkinsTest

LbUtils.Log._default_log_format = '%(asctime)s:' + LbUtils.Log._default_log_format


class Script(LbUtils.Script.PlainScript):
    '''
    Sends the message that a build has been done
    '''
    __usage__ = '%prog'
    __version__ = ''


    def defineOpts(self):
        '''Define options.'''
        from LbNightlyTools.Scripts.Common import addBasicOptions

        self.parser.add_option('-q', '--queue', action='store',
                               default=None,
                               help='Persistent queue in which to store the messages')
        self.parser.add_option('-j', '--jenkins', action='store_true',
                               default=False,
                               help='Stote the jobs to run in Jenkins format')

        addBasicOptions(self.parser)


    def main(self):
        '''
        Main function of the script.
        '''
        
        # Initializing the messenger and getting the actual list
        msg = LbMsg.TestMsg.TestMessenger()
        

        # Printing out or creating out files
        format = "test-params-{0}.txt"

        # Just printing out CSV by default
        if not self.options.jenkins:
            # In this case the callback just prints the message
            def processMsg(idx, testToRun):
                print ",".join([ str(idx) ] + testToRun)
        else:
            # Here we writeout the files for Jenkins
            def processMsg(idx, testToRun):
                (slot, buildId, project, config, group, env, runner, os_label) = testToRun
                self.log.warning("Job %d: %s" % (idx, ",".join(testToRun)))
                jenkins_test = JenkinsTest(slot, buildId, project, config,
                                           os_label, group, runner, env)
                with open(format.format(idx), 'w') as paramfile:
                    paramfile.writelines(jenkins_test.getParameterLines())
                    self.log.warning(format.format(idx))

        msg.processTestToRun(processMsg, queueName=self.options.queue)    
        
if __name__ == "__main__":
    sys.exit(Script().run())

